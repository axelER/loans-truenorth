# loans-truenorth

swagger: http://localhost:8080/swagger-ui/

Points to improve in order to have a robuster API

* Handling and support more http errors on controller layer
* Using spring custom validator or something similar to validate loans fields
* Use DTO's to expose data  