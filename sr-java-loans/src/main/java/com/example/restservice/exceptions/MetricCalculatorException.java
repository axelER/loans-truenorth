package com.example.restservice.exceptions;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class MetricCalculatorException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public MetricCalculatorException(String message) {
		super(message);
		log.error(message);
	}
	
	public MetricCalculatorException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}

}
