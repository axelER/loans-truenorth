package com.example.restservice.exceptions;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CalculateLoanMetricException extends RuntimeException {
	
	private static final long serialVersionUID = -4381815443173443491L;

	public CalculateLoanMetricException(String message) {
		super(message);
		log.error(message);
	}
	
	public CalculateLoanMetricException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}

}
