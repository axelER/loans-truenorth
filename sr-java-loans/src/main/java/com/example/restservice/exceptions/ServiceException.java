package com.example.restservice.exceptions;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ServiceException extends Exception {

	private static final long serialVersionUID = -4652937817564863413L;
	
	public ServiceException(String message) {
		super(message);
		log.error(message);
	}
	
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}

}
