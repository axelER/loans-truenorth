package com.example.restservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.extern.log4j.Log4j2;

@Log4j2
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class LoanControllerException extends RuntimeException {
	
	private static final long serialVersionUID = 2472163205020712058L;

	public LoanControllerException(String message) {
		super(message);
		log.error(message);
	}
	
	public LoanControllerException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}

}
