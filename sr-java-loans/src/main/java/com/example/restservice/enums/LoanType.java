package com.example.restservice.enums;

import com.example.restservice.model.Loan;

public enum LoanType {
	
	STUDENT("student", true),
	CONSUMER("consumer", true);
	
	private String description;
	
	private boolean available;
	
	LoanType(String description, boolean available) {
		this.description = description;
		this.available = available;
	}

	public String getDescription() {
		return description;
	}
	
	public boolean isAvailable() {
		return available;
	}

	public boolean isAvailable(Loan loan) {
		return this.available && this.getDescription().equalsIgnoreCase(loan.getType());
	}
	
}
