package com.example.restservice.daos;

import com.example.restservice.model.Loan;

public interface ILoanDAO {
	
	public Loan findById(Long id);
	
}
