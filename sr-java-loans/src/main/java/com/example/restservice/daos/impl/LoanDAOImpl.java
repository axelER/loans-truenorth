package com.example.restservice.daos.impl;

import org.springframework.stereotype.Component;

import com.example.restservice.daos.ILoanDAO;
import com.example.restservice.model.Loan;
import com.example.restservice.util.LoanGeneratonUtil;

@Component
public class LoanDAOImpl implements ILoanDAO {

	@Override
	public Loan findById(Long id) {
		return LoanGeneratonUtil.createLoan(id);
	}

}
