package com.example.restservice.model;

import lombok.Data;

@Data
public class Borrower {

	private String name;
	private Integer age;
	private Double annualIncome;
	private Boolean delinquentDebt;
	private Double annualDebt;
	private Integer creditHistory;

}