package com.example.restservice.model;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Loan data")
@Data
public class Loan {

	@NotNull(message = "Cannot be null.")
	@ApiModelProperty(value = "Loan id", required = true)
	private Long loanId;
	
	private Double requestedAmount;
	
	private Integer termMonths;
	
	private Double annualInterest;
	
	private String type;
	
	private Borrower borrower;

}