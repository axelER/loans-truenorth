package com.example.restservice.model;

import lombok.Data;

@Data
public class LoanMetric {

	private Double monthlyInterestRate;
	private Double monthlyPayment;

	public LoanMetric(double monthlyInterestRate, double monthlyPayment) {
		this.monthlyInterestRate = monthlyInterestRate;
		this.monthlyPayment = monthlyPayment;
	}

}