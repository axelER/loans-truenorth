package com.example.restservice.model.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response<T> implements Serializable {

	private static final long serialVersionUID = 5544647869629760864L;

	private Integer statusCode;
	private String statusText;
	private T data;
	private String error;

}
