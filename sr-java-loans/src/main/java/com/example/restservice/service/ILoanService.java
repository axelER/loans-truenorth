package com.example.restservice.service;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

import org.springframework.validation.annotation.Validated;

import com.example.restservice.exceptions.ServiceException;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;

@Validated
public interface ILoanService {
	
	public Loan getLoan(@Positive Long id) throws ServiceException;

	public LoanMetric calculateLoanMetric(@Valid Loan loan) throws ServiceException;

	public LoanMetric calculateLoanMetric(Long loanId) throws ServiceException;

	public Loan getMaxMonthlyPaymentLoan() throws ServiceException;

}
