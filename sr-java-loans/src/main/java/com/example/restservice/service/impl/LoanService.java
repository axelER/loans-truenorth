package com.example.restservice.service.impl;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.restservice.daos.ILoanDAO;
import com.example.restservice.metrics.LoanMetricFactory;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.service.ILoanService;
import com.example.restservice.util.LoanGeneratonUtil;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class LoanService implements ILoanService {
	
	private final LoanMetricFactory loanMetricFactory;
	private final ILoanDAO loanDAO;
	
	@Override
	public Loan getLoan(Long id) {
		return this.loanDAO.findById(id);
	}

	@Override
	public LoanMetric calculateLoanMetric(Loan loan) {
		if(!this.completeLoan(loan)) {
			return this.calculateLoanMetric(loan.getLoanId());
		}
		return this.loanMetricFactory.getInstance(loan).getLoanMetric(loan);
	}

	@Override
	public LoanMetric calculateLoanMetric(Long loanId) {
		Loan loan = this.getLoan(loanId);
		return this.calculateLoanMetric(loan);
	}

	@Override
	public Loan getMaxMonthlyPaymentLoan() {
		List<Loan> allLoans = LoanGeneratonUtil.getRandomLoans(20L);
		Comparator<Loan> comparator = (a, b) -> Double.compare(
										this.calculateLoanMetric(a).getMonthlyPayment(), 
										this.calculateLoanMetric(b).getMonthlyPayment());
		Optional<Loan> optLoan = allLoans.stream().max(comparator);
		return optLoan.get();
	}
	
	//It could be much better using some spring conditional validator
	//this approach is not scalable
	private boolean completeLoan(Loan loan) {
		return loan.getAnnualInterest() != null && loan.getBorrower() != null && 
				loan.getRequestedAmount() != null && loan.getTermMonths() != null 
				&& loan.getType() != null;
	}

}
