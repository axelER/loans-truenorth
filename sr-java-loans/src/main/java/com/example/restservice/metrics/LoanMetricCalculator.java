package com.example.restservice.metrics;

import java.math.BigDecimal;
import java.math.MathContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.restservice.daos.ILoanDAO;
import com.example.restservice.exceptions.CalculateLoanMetricException;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;

public class LoanMetricCalculator {
	
	@Autowired 
	ILoanDAO loanDAO;
	
	private static final BigDecimal MONTHS_OF_THE_YEAR = new BigDecimal(12);
	
	public LoanMetric getLoanMetric(Loan loan) {
		LoanMetric loanMetric = null;
		Double monthlyInterestRate = this.getMonthlyInterestRate(loan);
		Double monthlyPayment = this.getMonthlyPayment(loan);
		loanMetric = new LoanMetric(monthlyInterestRate, monthlyPayment);
		return loanMetric;
	}
	
	private Double getMonthlyInterestRate(Loan loan) {
		//It´s better if we use BigDecimal everywhere otherwise we could have 
		//problems when we need rounding numbers
		Double monthlyInterestRate = 0D;
		BigDecimal annualInterest = new BigDecimal(loan.getAnnualInterest());
		//It's going to be more flexible is we use the loanDAO to get this value, because
		//we can receive on runtime specific values depending of loan type
		BigDecimal divisor = new BigDecimal(100);
		//We can lose information at this point
		//it's important to precisely define the rounding logic
		//(annualInterest /12 ) / 100
		MathContext mc = new MathContext(5);
		monthlyInterestRate = annualInterest.divide(MONTHS_OF_THE_YEAR, mc)
								.divide(divisor, mc)
								.doubleValue();
		return monthlyInterestRate;
	}
	
	private Double getMonthlyPayment(Loan loan) {
		//It´s better if we use BigDecimal everywhere otherwise we could have 
		//problems when we need rounding numbers
		Double monthlyPayment = 0D;
		BigDecimal requestedAmount = new BigDecimal(loan.getRequestedAmount());
		BigDecimal monthlyInterestRate = new BigDecimal(this.getMonthlyInterestRate(loan));
		
		//It's going to be more flexible is we use the loanDAO to get this value, because
		//we can receive on runtime specific values depending of loan type
		BigDecimal addend = new BigDecimal(1);
		BigDecimal factor = new BigDecimal(-1);
		BigDecimal termMonths = new BigDecimal(loan.getTermMonths());
		
		//(requestedAmount * monthlyInterestRate)
		BigDecimal dividend = requestedAmount.multiply(monthlyInterestRate);
		
		//(1 + monthlyInterestRate)
		BigDecimal base = addend.add(monthlyInterestRate);
		
		//We can lose information at this point
		//it's important to precisely define the rounding logic
		//((-1) * termMonths)
		Integer power = factor.multiply(termMonths).intValue();
		
		//to avoid 0^0 or 0^-a
		if(base.equals(BigDecimal.ZERO) && (power == 0 || power < 0)) {
			throw new CalculateLoanMetricException("An error occurred while calculating loan metric");
		}
		
		//(1 + monthlyInterestRate) ^ ((-1) * termMonths))
		MathContext mc = new MathContext(5);
		BigDecimal pow = base.pow(power, mc);
		
		//(1 - (1 + monthlyInterestRate)^((-1) * termMonths) )
		BigDecimal divisor = addend.subtract(pow);
		
		//We can lose information at this point
		//it's important to precisely define the rounding logic
		//(requestedAmount * monthlyInterestRate) / (1 - (1 + monthlyInterestRate)^((-1) * termMonths) )
		monthlyPayment = dividend.divide(divisor, mc).doubleValue();
		
		return monthlyPayment;
	}
	
	public final void setLoanDAO(ILoanDAO loanDAO) {
		this.loanDAO = loanDAO;
	}

}
