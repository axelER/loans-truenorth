package com.example.restservice.metrics.impl;

import org.springframework.stereotype.Component;

import com.example.restservice.enums.LoanType;
import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.metrics.LoanMetricCalculator;
import com.example.restservice.model.Borrower;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;

@Component
public class StudentLoanMetricCalculator extends LoanMetricCalculator implements ILoanMetricCalculator {
	
	private static final LoanType type = LoanType.STUDENT;
	
	@Override
	public boolean isSupported(Loan loan, LoanType type) {
		return ILoanMetricCalculator.super.isSupported(loan, type) && 
				hasValidAge(loan.getBorrower());
	}
	
	@Override
	public LoanMetric getLoanMetric(Loan loan) {
		LoanMetric loanMetric = super.getLoanMetric(loan);
		Double factor = 0.8;
		Double monthlyPayment = factor * loanMetric.getMonthlyPayment();
		loanMetric.setMonthlyPayment(monthlyPayment);
		return loanMetric;
	}
	
	private boolean hasValidAge(Borrower borrower) {
		return borrower.getAge() > 18 && borrower.getAge() < 30; 
	}
	
	public LoanType getLoanType() {
		return type;
	}
	
}