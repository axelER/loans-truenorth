package com.example.restservice.metrics.impl;

import org.springframework.stereotype.Component;

import com.example.restservice.enums.LoanType;
import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.metrics.LoanMetricCalculator;

@Component
public class ConsumerLoanMetricCalculator extends LoanMetricCalculator implements ILoanMetricCalculator {
	
	private static final LoanType type = LoanType.CONSUMER;

	public LoanType getLoanType() {
		return type;
	}
	
}
