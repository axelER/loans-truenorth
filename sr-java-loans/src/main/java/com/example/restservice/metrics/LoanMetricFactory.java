package com.example.restservice.metrics;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.restservice.exceptions.MetricCalculatorException;
import com.example.restservice.model.Loan;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class LoanMetricFactory {
	
	private final List<ILoanMetricCalculator> loanCalculators;

	public ILoanMetricCalculator getInstance(Loan loan) {
		Optional<ILoanMetricCalculator> optCalculator = loanCalculators.stream()
															.filter(c -> c.isSupported(loan, c.getLoanType()))
															.findAny();
		return optCalculator.orElseThrow(
				() -> new MetricCalculatorException(
						"There is no metric calculator for this loan: " + loan.getType()));
	}

}