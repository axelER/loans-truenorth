package com.example.restservice.controller;

import org.springframework.http.MediaType;

import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.model.response.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "/loans")
public interface ILoanController {
	
    /**
     * @param loan id
     * @return Loan
     */
    @ApiOperation(value = "Get a loan by id",
            response = Loan.class,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request loaded successfully", response = Loan.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 500, message = "An error has occurred in the service")
    })
    Response<Loan> getLoan(@ApiParam(value = "Loan id", required = true) Long loanId);
	
    /**
     * Retrieve the loan metric based on a loan.
     * Loan id is a mandatory field. 
     * The rest of the fields are optionals unless you complete one of them, 
     * in that case requestedAmount, termMonths, annualInterest, type and borrower
     * cannot be empty.
     * 
     * @param loan
     * @return LoanMetric
     */
    @ApiOperation(value = "Retrieve the loan metric based on a loan. Loan id is a mandatory field. "
    				+ "The rest of the fields are optionals unless you complete one of them, in that"
    				+ " case requestedAmount, termMonths, annualInterest, type and borrower cannot be empty.",
            response = LoanMetric.class,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Loan metric created successfully", response = LoanMetric.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 500, message = "An error has occurred in the service")
    })
    Response<LoanMetric> calculateLoanMetric(@ApiParam(value = "Loan id", required = true) Loan loan);
	
    /**
     * Retrieve the loan with the maximum monthly payment
     * 
     * @return Loan
     */
    @ApiOperation(value = "Retrieve the loan with the maximum monthly payment",
            response = Loan.class,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request loaded successfully", response = Loan.class),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 500, message = "An error has occurred in the service")
    })
    Response<Loan> getMaxMonthlyPaymentLoan();

}
