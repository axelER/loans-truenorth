package com.example.restservice.controller.impl;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.restservice.controller.ILoanController;
import com.example.restservice.exceptions.LoanControllerException;
import com.example.restservice.exceptions.ServiceException;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.model.response.Response;
import com.example.restservice.service.ILoanService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/loans")
@RequiredArgsConstructor
public class LoanController implements ILoanController {
	
	private final ILoanService loanService;

	@GetMapping("/{loanId}")
	@Override
	public Response<Loan> getLoan(@PathVariable Long loanId) {
		Loan loan;
		Response response;
		try {
			loan = this.loanService.getLoan(loanId);
			response = new Response<Loan>(
						HttpStatus.OK.value(), 
						HttpStatus.OK.getReasonPhrase(), 
						loan,
						"");
		} catch (ServiceException e) {
			log.error("An error has occurred in loanService.getLoan(loanId)", e);
			throw new LoanControllerException("An error has occurred in loanService.getLoan(loanId)", e);
		}  catch (ConstraintViolationException ex) {
			log.error("An error has occurred in lloanService.getLoan(loanId)", ex);
			throw new LoanControllerException("An error has occurred in loanService.getLoan(loanId)", ex);
		}
		return response;
	}

	@PostMapping("/metric")
	@Override
	public Response<LoanMetric> calculateLoanMetric(@RequestBody Loan loan) {
		LoanMetric loanMetric;
		Response response;
		try {
			loanMetric = this.loanService.calculateLoanMetric(loan);
			response = new Response<LoanMetric>(
					HttpStatus.OK.value(), 
					HttpStatus.OK.getReasonPhrase(), 
					loanMetric,
					"");
		} catch (ServiceException e) {
			log.error("An error has occurred in loanService.calculateLoanMetric(loan)", e);
			throw new LoanControllerException("An error has occurred in loanService.calculateLoanMetric(loan)", e);
		}  catch (ConstraintViolationException ex) {
			log.error("An error has occurred in loanService.calculateLoanMetric(loan)", ex);
			throw new LoanControllerException("An error has occurred in loanService.calculateLoanMetric(loan)", ex);
		}
		return response;
	}

	@GetMapping("/maxMonthlyPayment")
	@Override
	public Response<Loan> getMaxMonthlyPaymentLoan() {
		Loan loan;
		Response response;
		try {
			loan = this.loanService.getMaxMonthlyPaymentLoan();
			response = new Response<Loan>(
					HttpStatus.OK.value(), 
					HttpStatus.OK.getReasonPhrase(), 
					loan,
					"");
		} catch (ServiceException e) {
			log.error("An error has occurred in loanService.getMaxMonthlyPaymentLoan()", e);
			throw new LoanControllerException("An error has occurred in loanService.getMaxMonthlyPaymentLoan()", e);
		}
		return response;
	}

}
