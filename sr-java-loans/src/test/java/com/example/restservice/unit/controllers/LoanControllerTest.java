package com.example.restservice.unit.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.example.restservice.controller.impl.LoanController;
import com.example.restservice.model.Loan;
import com.example.restservice.service.ILoanService;
import com.example.restservice.util.LoanGeneratonUtil;

@WebMvcTest(controllers = LoanController.class)
public class LoanControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ILoanService loanService;
	
	@Test
	void when_validInput_on_getLoan_thenReturn200() throws Exception {
		//setup
		Long loanId = 5l;
		Loan loan = LoanGeneratonUtil.createLoan(loanId);
		
		//execute
		when(loanService.getLoan(loanId)).thenReturn(loan);
		mockMvc.perform(get("/loans/"+loanId)).andExpect(status().isOk());
			
	}

}
