package com.example.restservice.unit.metrics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.restservice.enums.LoanType;
import com.example.restservice.metrics.impl.ConsumerLoanMetricCalculator;
import com.example.restservice.metrics.impl.StudentLoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.util.LoanGeneratonUtil;

@ExtendWith(MockitoExtension.class)
public class LoanMetricCalculatorTest {
	
	@InjectMocks
	private ConsumerLoanMetricCalculator consumerLoanMetricCalculator;
	
	@InjectMocks
	private StudentLoanMetricCalculator studentLoanMetricCalculator;
	
	@Test
	void when_consumerLoanCreated_consumerLoanMetric_supportIt() {
		//setup
		Long loanId = 5l;
		Loan loan = LoanGeneratonUtil.createLoan(loanId);
		
		//assert
		Assertions.assertEquals(LoanType.CONSUMER.getDescription(), loan.getType(), "Error building loan");
		Assertions.assertTrue(consumerLoanMetricCalculator.isSupported(loan, LoanType.CONSUMER), "consumerLoanMetric doesn't support it");
	}
	
	@Test
	void when_studentLoanCreated_consumerLoanMetric_supportIt() {
		//setup
		Long loanId = 4l;
		Loan loan = LoanGeneratonUtil.createLoan(loanId);
		
		//assert
		Assertions.assertEquals(LoanType.STUDENT.getDescription(), loan.getType(), "Error building loan");
		Assertions.assertTrue(studentLoanMetricCalculator.isSupported(loan, LoanType.STUDENT), "studentLoanMetric doesn't support it");
	}
	
	@Test
	void consumerMonthlyInterestRateCalculatorTest() {
		//setup
		Loan loan = new Loan();
		loan.setLoanId(1L);
		loan.setRequestedAmount(10000d);
		loan.setTermMonths(24);
		loan.setAnnualInterest(6d);
		loan.setType(LoanType.CONSUMER.getDescription());
		loan.setBorrower(LoanGeneratonUtil.createLoan(4l).getBorrower());
		
		Assertions.assertTrue(consumerLoanMetricCalculator.getLoanMetric(loan).getMonthlyInterestRate() == 0.005d, "error calculating monthly interested rate");
	}

}
