package com.example.restservice.unit.services;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.restservice.daos.ILoanDAO;
import com.example.restservice.metrics.LoanMetricFactory;
import com.example.restservice.model.Loan;
import com.example.restservice.service.impl.LoanService;
import com.example.restservice.util.LoanGeneratonUtil;

@ExtendWith(MockitoExtension.class)
public class LoanServiceTest {
	
	@Mock
	private LoanMetricFactory loanMetricFactory;
	
	@Mock
	private ILoanDAO loanDAO;
	
	@InjectMocks
	private LoanService loanService;
	
	@Test
	void getLoan() {
		//setup
		Long loanId = 5l;
		Loan loan = LoanGeneratonUtil.createLoan(loanId);
		
		when(loanDAO.findById(loanId)).thenReturn(loan);
		
		//execute
		Loan result = loanService.getLoan(loanId);
		
		//assert
		Assertions.assertEquals(loan, result, "Loans are differents");
	}

}
